package com.greatlearning.designpattern1.singletonpattern;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.greatlearning.designpattern2.builderpattern.BankAccount;

public class JDBCSingleton {

	private static final Logger logger = Logger.getLogger(JDBCSingleton.class.getName());

	private JDBCSingleton() {
	}

	private static JDBCSingleton jdbc;

	public static JDBCSingleton getInstance() {
		if (jdbc == null) {
			synchronized (JDBCSingleton.class) {
				if (jdbc == null) {
					jdbc = new JDBCSingleton();
				}
			}
		}
		return jdbc;
	}

	private static Connection getConnection() throws ClassNotFoundException, SQLException {

		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "sql123");

	}

	public int createAccount(BankAccount bankAccount) throws SQLException {
		logger.info("Inside Create Account Method");
		Connection conn = null;
		PreparedStatement ps = null;
		int recordCounter = 0;
		try {
			conn = JDBCSingleton.getConnection();
			ps = conn.prepareStatement(
					"insert into bankaccountdetails(bankaccountNumber,accounttype,branch, balance, atmtransactions,emiSchedule)values(?,?,?,?,null,null)");
			ps.setString(1, bankAccount.getBankAccountNo());
			ps.setString(2, bankAccount.getAccountType().getType());
			ps.setString(3, bankAccount.getBranch());
			ps.setDouble(4, bankAccount.getBalance());
			recordCounter = ps.executeUpdate();

		} catch (Exception e) {
			logger.info("Exception occurred in create account method " + e.getMessage());
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return recordCounter;
	}

	public void viewAccount(String bankaccountNumber) throws SQLException {
		logger.info("Inside View Account Method");
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			con = JDBCSingleton.getConnection();
			ps = con.prepareStatement("select * from bankaccountdetails where bankaccountNumber=?");
			ps.setString(1, bankaccountNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				logger.info("Bank Account Number= " + rs.getInt(1) + "\t" + "Account Type= " + rs.getString(2) + "\t"
						+ "Branch= " + rs.getString(3) + "\t" + "Balance= " + rs.getInt(4));

			}

		} catch (Exception e) {
			logger.info("Exception occurred in create account method " + e.getMessage());
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		}
	}

}