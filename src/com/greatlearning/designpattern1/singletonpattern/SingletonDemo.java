package com.greatlearning.designpattern1.singletonpattern;

import com.greatlearning.designpattern2.builderpattern.AccountType;
import com.greatlearning.designpattern2.builderpattern.BankAccount;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SingletonDemo {
	private static final Logger logger = Logger.getLogger(SingletonDemo.class.getName());

	public static void main(String[] args) {
		JDBCSingleton jdbcConnection = JDBCSingleton.getInstance();
		List<String> transactions = new ArrayList<>();
		transactions.add("TR12");
		transactions.add("TR25");
		BankAccount bankAccount = new BankAccount.BankAccountBuilder("1234", AccountType.SAVING, "Chennai", 45.00)
				.setEmiSchedule("Monday").setAtmTransactions(transactions).build();
		try {

			int i = jdbcConnection.createAccount(bankAccount);
			if (i > 0) {
				logger.info("Data has been inserted successfully");
			} else {
				logger.info("Data has not been inserted ");
			}

			// To view the inserted record
			jdbcConnection.viewAccount(bankAccount.getBankAccountNo());

		} catch (SQLException sqlException) {
			logger.info("SQL Exception occurred " + sqlException.getMessage());
		} catch (Exception e) {
			logger.info("Exception occurred " + e.getMessage());
		}
	}
}
