package com.greatlearning.designpattern3.factorypattern;

public class GbrCurrency implements Currency {
	@Override
	public Double convertToINR(Double amount) {
		return amount * 60;
	}
}
