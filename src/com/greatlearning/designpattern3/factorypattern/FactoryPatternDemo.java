package com.greatlearning.designpattern3.factorypattern;

import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Logger;

public class FactoryPatternDemo {
	private static final Logger logger = Logger.getLogger(FactoryPatternDemo.class.getName());

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		logger.info("Enter the source currency");
		CurrencyType sourceCurrency = CurrencyType.getByCurrency(sc.nextLine());
		if (!Objects.isNull(sourceCurrency)) {
			logger.info("Enter the source amount");
			Double amount = sc.nextDouble();

			CurrencyFactory currencyFactory = new CurrencyFactory();
			Currency currency = currencyFactory.getCurrency(sourceCurrency);

			logger.info("Equalent INR Currency Value: Rs." + currency.convertToINR(amount));
		} else {
			logger.info("Please select either GBR or USD");
		}

	}

}
