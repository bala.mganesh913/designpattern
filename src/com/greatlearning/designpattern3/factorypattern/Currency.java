package com.greatlearning.designpattern3.factorypattern;

public interface Currency {
	Double convertToINR(Double amount);
}
