package com.greatlearning.designpattern3.factorypattern;

public class UsdCurrency implements Currency {
	@Override
	public Double convertToINR(Double amount) {
		return amount * 74;
	}
}
