package com.greatlearning.designpattern3.factorypattern;

import org.apache.commons.lang3.StringUtils;

import java.util.EnumSet;

public enum CurrencyType {

	USD("USD"), GBR("GBR");

	private String currency;

	CurrencyType(String currency) {
		this.currency = currency;
	}

	public String getCurrency() {
		return this.currency;
	}

	public static CurrencyType getByCurrency(String currency) {
		return EnumSet.allOf(CurrencyType.class).stream()
				.filter(value -> StringUtils.equals(value.getCurrency(), currency)).findFirst().orElse(null);
	}
}