package com.greatlearning.designpattern3.factorypattern;

import java.util.logging.Logger;

public class CurrencyFactory {
	private static final Logger logger = Logger.getLogger(CurrencyFactory.class.getName());

	public Currency getCurrency(CurrencyType currency) {

		switch (currency) {
		case USD:
			return new UsdCurrency();
		case GBR:
			return new GbrCurrency();
		default:
			logger.info("Invalid Currency Type");
			return null;
		}
	}
}
