package com.greatlearning.designpattern2.builderpattern;

public enum AccountType {

	SAVING("Saving"), CURRENT("Current");

	String accountType;

	AccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getType() {
		return this.accountType;
	}
}
