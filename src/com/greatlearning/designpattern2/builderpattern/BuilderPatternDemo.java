package com.greatlearning.designpattern2.builderpattern;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class BuilderPatternDemo {
	private static final Logger logger = Logger.getLogger(BuilderPatternDemo.class.getName());

	public static void main(String[] args) {

		List<String> transactions = new ArrayList<>();
		transactions.add("TR12");
		transactions.add("TR25");

		BankAccount bankAccount = new BankAccount.BankAccountBuilder("1234", AccountType.SAVING, "Chennai", 45.00)
				.setEmiSchedule("Monday").setAtmTransactions(transactions).build();

		logger.info(bankAccount.toString());
	}

}
