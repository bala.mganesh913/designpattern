package com.greatlearning.designpattern2.builderpattern;

import java.util.List;

public class BankAccount {

	// mandatory fields
	private String bankAccountNo;
	private AccountType accountType;
	private String branch;
	private Double balance;

	// optional fields
	private List<String> atmTransactions;
	private String emiSchedule;

	public BankAccount(BankAccountBuilder bankAccountBuilder) {
		this.bankAccountNo = bankAccountBuilder.bankAccountNo;
		this.accountType = bankAccountBuilder.accountType;
		this.branch = bankAccountBuilder.branch;
		this.balance = bankAccountBuilder.balance;

		this.atmTransactions = bankAccountBuilder.atmTransactions;
		this.emiSchedule = bankAccountBuilder.emiSchedule;
	}

	public static class BankAccountBuilder {
		// mandatory fields
		private String bankAccountNo;
		private AccountType accountType;
		private String branch;
		private Double balance;

		// optional fields
		private List<String> atmTransactions;
		private String emiSchedule;

		public BankAccountBuilder(String bankAccountNo, AccountType accountType, String branch, Double balance) {
			this.bankAccountNo = bankAccountNo;
			this.accountType = accountType;
			this.branch = branch;
			this.balance = balance;
		}

		public BankAccountBuilder setAtmTransactions(List<String> atmTransactions) {
			this.atmTransactions = atmTransactions;
			return this;
		}

		public BankAccountBuilder setEmiSchedule(String emiSchedule) {
			this.emiSchedule = emiSchedule;
			return this;
		}

		public BankAccount build() {
			return new BankAccount(this);
		}

	}

	public String getBankAccountNo() {
		return bankAccountNo;
	}

	public void setBankAccountNo(String bankAccountNo) {
		this.bankAccountNo = bankAccountNo;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public List<String> getAtmTransactions() {
		return atmTransactions;
	}

	public void setAtmTransactions(List<String> atmTransactions) {
		this.atmTransactions = atmTransactions;
	}

	public String getEmiSchedule() {
		return emiSchedule;
	}

	public void setEmiSchedule(String emiSchedule) {
		this.emiSchedule = emiSchedule;
	}

	@Override
	public String toString() {
		return "BankAccount{" + "bankAccountNo='" + bankAccountNo + '\'' + ", accountType=" + accountType + ", branch='"
				+ branch + '\'' + ", balance=" + balance + ", atmTransactions=" + atmTransactions + ", emiSchedule='"
				+ emiSchedule + '\'' + '}';
	}
}
